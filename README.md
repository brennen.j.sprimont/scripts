Scripts
=======

This repository is a collection of scripts to automate common tasks.
I don't expect anyone else will every contribute to this, but feel free
to cherry pick anything that seems useful.