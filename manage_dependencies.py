##
# @file
#
# Manages dependencies for a project.
#
#

import sys
import os
import logging
import argparse


def manage_arguments():
   """
   Adds the argument definitions and parses the arguments passed into this script file.
   """
   argument_parser = argparse.ArgumentParser("TODO")
   argument_parser.add_argument( "-c", "--clean",
                                 default="False",
                                 help="Cleans the dependencies out of the project. Default: %(default)s)",
                                 action="store_true")
   argument_parser.add_argument( "-f", "--file",
                                 default="dependencies.json",
                                 help="Configuration file containing the projects dependency information. Default: %(default)s)")
   argument_parser.add_argument( "-g", "--generate-template",
                                 default=False,
                                 help="Generates a dependency configuration template file and exits. Default: %(default)s)",
                                 action="store_true")
   arguments = argument_parser.parse_args()
   print("Arguments: ")
   print("   clean: ", arguments.clean)
   print("   file: ", arguments.file)
   print("   generate_template: ", arguments.generate_template)
   return arguments


def main():
   """
   Main entry into the project generation script.
   """
   arguments = manage_arguments()
   if arguments.generate_template == True:
      print("TODO: generate template.")
      sys.exit(0)
   if arguments.clean == True:
      print("TODO: clean dependencies directory.")
   print("TODO: read and parse dependencies configuration file (json? yaml? xml?).")
   print("TODO: check to see if the dependency is already present locally. If so, check version.")
   print("TODO: retrieve dependencies if needed. Add a versions file to the dependencies directory.")


if __name__ == "__main__":
   logging.basicConfig(format="%(asctime)s | %(name)s | %(levelname)s | %(message)s", stream=sys.stdout, level=logging.DEBUG)
   logging.info("Starting dependency manager...")
   main()
   logging.info("Ending dependency manager...")

