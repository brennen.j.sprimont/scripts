#!/usr/bin/python3
##
# @file
#
# Generates project based on arguments. To update or add projects, edit the
# templates in the "./resources/templates/projects" directory.
#
# @example
#  ./generate_project.py --template c-generic --output-directory ~/projects --name my_new_c_project
#
#


# TODO: add linter? add valgrind scripts? add debug scripts? 
#


import sys
import os
import shutil
import errno
import argparse
import pathlib
import json
import find_in_files
import logging

THIS_SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))


def manage_arguments():
   """
   Adds definitions for the 
   """
   argument_parser = argparse.ArgumentParser("Generates projects based on templates.")
   argument_parser.add_argument( "-o", "--output-directory",
                                 default="./",
                                 help="Directory to create the new project in. Default: %(default)s)")
   argument_parser.add_argument( "-t", "--template",
                                 choices=["c-generic", "c-embedded", "cpp"],
                                 default="c-generic",
                                 help="Project template to generate. Default: %(default)s)")
   argument_parser.add_argument( "-n", "--name",
                                 default="new_project",
                                 help="Name of the new project. Default: %(default)s)")
   arguments = argument_parser.parse_args()
   print("Arguments: ")
   print("   output-directory: ", arguments.output_directory)
   print("   template: ", arguments.template)
   print("   name: ", arguments.name)
   return arguments


def find_template_directory(template_identifier):
   """
   Finds the project template directory to copy based on the template identifier.
   @param template_identifier Identifier used to find the template directory. See the usage
      of the script for a complete list of options.
   @return The path to the specified project template.
   """
   template_directory_path = ""
   if template_identifier == "c-generic":
      template_directory_path = "{this_script_path}/resources/templates/projects/c/generic".format(this_script_path=THIS_SCRIPT_PATH)
   else:
      sys.exit("Error! Template doesn't exist!")

   print("Template Directory: ", template_directory_path)
   return template_directory_path


def copy_project_template(template_source_directory, output_directory, project_name):
   """
   Copies the specified project template to the specified output directory with the specified name.
   @param template_source_directory Directory of the project template to copy.
   @param output_directory          Directory to place the copied project template.
   @param project_name              Name of the new project.
   @return The absolute path to the new project.
   """
   project_absolute_path = output_directory + "/" + project_name
   print("Project Path: ", project_absolute_path)
   if os.path.exists(project_absolute_path) == False:
      shutil.copytree(template_source_directory, project_absolute_path)
      return project_absolute_path
   else:
      sys.exit("Error! Output directory already exists! Choose a directory path that doesn't exist.")


def replace_keywords(directory, replace_dictionary):
   """
   Replaces all keywords in all files in the specified directory.
   @param directory           The directory to replace keywords in.
   @param replace_dictionary  The dictionary containing the keywords and values to replace.
   """
   print("TODO: replace keywords.")


def generate_todo_file(directory):
   """
   Generates a file containing a list of manual TODOs after generating a new project.
   @param directory  The directory to search for TODOs in and generate the TODO file in.
   @return Path to the newly created TODO file.SS
   """
   find_results = find_in_files.find_in_files(directory, "*", "{{TODO}}")
   todo_file_path = directory + "/TODO.txt"
   with open(todo_file_path, 'a') as todo_file:
      todo_file.write(find_results)
      todo_file.write("\n TODO: DELETE THIS FILE. \n")
   print("TODO file: ", todo_file_path)
   return(todo_file_path)


def main():
   """
   Main entry into the project generation.
   """
   arguments = manage_arguments()
   template_source_directory = find_template_directory(arguments.template)
   new_project_path = copy_project_template(template_source_directory, arguments.output_directory, arguments.name)
   keyword_replace_dictionary_path = "{this_script_path}/resources/templates/projects/keyword_replace_dictionary.json".format(this_script_path=THIS_SCRIPT_PATH)
   print(keyword_replace_dictionary_path)
   #keyword_replace_dictionary = json.loads(open(keyword_replace_dictionary_path))
   #replace_keywords(new_project_path, keyword_replace_dictionary)
   generate_todo_file(new_project_path)


if __name__ == "__main__":
   logging.basicConfig(format="%(asctime)s | %(name)s | %(levelname)s | %(message)s", stream=sys.stdout, level=logging.DEBUG)
   logging.info("Starting project generator...")
   main()
   logging.info("Ending project generator...")