/**
 * @file {{file_name}}
 * 
 * 
 */

#include <stdio.h>
#include <stdint.h>

#include "{{file_name}}.h"


int main (int argc, char **argv)
{
   int32_t x = function(1);
   printf("%d\n", x);
   return 0;
}

int32_t function(uint8_t param)
{
   return (int32_t)param;
}