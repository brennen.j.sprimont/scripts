#!/usr/bin/python3
##
# @file
#
# Generates the projects doxygen documentation. See the configuration
# file to modify the doxygen output. The output will be ignored from
# the git repository and deleted upon cleaning the project.
#

import sys
import os
import logging
from subprocess import call



def generate_doxygen(doxygen_configuration_file):
   """
   Generates the projects doxygen documentation.
   """
   pathlib.Path('../output/doxygen').mkdir(parents=True, exist_ok=True) 
   call(["doxygen", doxygen_configuration_file])

if __name__ == "__main__":
   logging.basicConfig(format="%(asctime)s | %(name)s | %(levelname)s | %(message)s", stream=sys.stdout, level=logging.DEBUG)
   generate_doxygen("../documentation/doxygen/doxygen.config")