#!/usr/bin/python3
##
# @file
#
# Cleans the project by remove generated build and output files.
#

BUILD_DIRECTORY = "../build"
OUTPUT_DIRECTORY = "../output"

import sys
import os
import shutil
import logging


def clean():
   """
   Cleans out the projects build and output files.
   """
   print("Cleaning project... Deleting build and output directory...")
   shutil.rmtree(BUILD_DIRECTORY)
   shutil.rmtree(OUTPUT_DIRECTORY)


if __name__ == "__main__":
   logging.basicConfig(format="%(asctime)s | %(name)s | %(levelname)s | %(message)s", stream=sys.stdout, level=logging.DEBUG)
   clean()
