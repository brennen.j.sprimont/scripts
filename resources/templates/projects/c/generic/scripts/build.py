#!/usr/bin/python3
##
# @file
#
# Builds the project by performing cmake and make.
# TODO: only perform a cmake if files are added or removed.
#

import sys
import os
import pathlib
import subprocess
import logging

def build():
   """
   Builds the CMake and Make project.
   """
   print('Building {{project_name}}...')
   pathlib.Path('../build').mkdir(parents=True, exist_ok=True) 
   pathlib.Path('../output').mkdir(parents=True, exist_ok=True) 
   os.chdir('../build')
   subprocess.call(['cmake', '..'])
   subprocess.call(['make'])

if __name__ == "__main__":
   logging.basicConfig(format="%(asctime)s | %(name)s | %(levelname)s | %(message)s", stream=sys.stdout, level=logging.DEBUG)
   build()